# google-spreadsheet-vue-i18n

## Usage

Generates vue-i18n data files using external google spreadsheet

Simple usage:

```
npm install google-spreadsheet-vue-i18n --save-dev
npm run google-spreadsheet-vue-i18n --spreadsheet=[GOOGLE_SPREADSHEET_ID] --sheet=[SHEET_NAME] --format=js --output=src/i18n/i18nData.js
```

or without installation:

```
npx google-spreadsheet-vue-i18n --spreadsheet=[GOOGLE_SPREADSHEET_ID] --sheet=[SHEET_NAME] --format=js --output=src/i18n/i18nData.js
```

Options:

- `--spreadsheet`: Google spreadsheet id taken from share URL
- `--sheet`: Google spreadsheet content sheet name
- `--format`: (`js` or `json`) Output format, js will add the export default ceremonial
- `--output`: Destination file or "stdout" to display the result

## Managing translations in google spreadsheet

Create a spreadsheet of this form

```
| key       | en   | fr      | es      |
+-----------+------+---------+---------+
| Foo.Bar   | Bar  | Chose   | Cosa    |
| User.Home | Home | Accueil | Entrada |
```

The first column is reserved to the translation key then one column for each language.
The header is important.

Click SHARE button, copy the share URL (can view is ok), it will be of the form :

https://docs.google.com/spreadsheets/d/[SPREADSHEET_ID]/edit?usp=sharing

Copy the SPREADSHEET_ID part and also take note of the Sheet's name you are editing then use them to invoke the script.

You can share the spreadsheet with your team so anyone can edit it, just execute the script once in a while or automatically in your CI system to update the translations.
